__version__ = '0.3.7'

from nisnap import snap
from nisnap import xnat
from nisnap.snap import plot_segment
__all__ = ['snap', 'xnat']
